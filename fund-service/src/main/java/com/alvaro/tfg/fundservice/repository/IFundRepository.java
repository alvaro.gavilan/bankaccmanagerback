package com.alvaro.tfg.fundservice.repository;

import com.alvaro.tfg.fundservice.model.Fund;

import java.util.List;

public interface IFundRepository {

    public List<Fund> getAllFunds();

    public Fund getFundById(int id);

    public Fund getFundByName(String name);

    public List<Fund> getActiveFunds();

    public List<Fund> getInactiveFunds();

    public int addFund(Fund fund);

    public int deleteFund(int id);

    public int reactivateFund(int id);
}
