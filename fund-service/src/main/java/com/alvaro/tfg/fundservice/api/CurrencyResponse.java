package com.alvaro.tfg.fundservice.api;

import lombok.Data;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Data
public class CurrencyResponse {

    private String name;

    private String symbol;

    private String code;

    public int getCurrencyIdByCode(String currencyCode) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8092/currencies/getCurrencyId/";
        try {
            return restTemplate.getForObject(url + currencyCode, Integer.class);
        } catch (HttpClientErrorException e) {
            return 0;
        }
    }

}
