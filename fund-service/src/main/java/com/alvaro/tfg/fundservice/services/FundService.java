package com.alvaro.tfg.fundservice.services;

import com.alvaro.tfg.fundservice.api.CurrencyResponse;
import com.alvaro.tfg.fundservice.api.FundRequestResponse;
import com.alvaro.tfg.fundservice.mappers.FundMapper;
import com.alvaro.tfg.fundservice.model.Fund;
import com.alvaro.tfg.fundservice.repository.IFundRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FundService {

    @Autowired
    private IFundRepository repository;

    private final RestTemplate REST_TEMPLATE = new RestTemplate();

    public List<Fund> getAllFunds() {
        return repository.getAllFunds();
    }

    public CurrencyResponse getCurrencyById(int currencyId) {
        String url = "http://localhost:8092/currencies/id/";
        return REST_TEMPLATE.getForObject(url + currencyId, CurrencyResponse.class);
    }

    public FundRequestResponse getFundById(int id) {
        Fund fund = repository.getFundById(id);
        CurrencyResponse currency = getCurrencyById(fund.getCurrencyId());
        return FundMapper.mapFromFundToFundResponse(fund, currency);
    }

    public FundRequestResponse getFundByName(String name) {
        Fund fund = repository.getFundByName(name);
        CurrencyResponse currency = getCurrencyById(fund.getCurrencyId());
        return FundMapper.mapFromFundToFundResponse(fund, currency);
    }

    public int getFundIdByName(String name) {
        Fund fund = repository.getFundByName(name);
        return fund.getId();
    }

    public List<FundRequestResponse> getActiveFunds() {
        return repository.getActiveFunds().stream().map(fund -> {
            CurrencyResponse currency = getCurrencyById(fund.getCurrencyId());
            return FundMapper.mapFromFundToFundResponse(fund, currency);
        }).collect(Collectors.toList());
    }

    public List<FundRequestResponse> getInactiveFunds() {
        return repository.getInactiveFunds().stream().map(fund -> {
            CurrencyResponse currency = getCurrencyById(fund.getCurrencyId());
            return FundMapper.mapFromFundToFundResponse(fund, currency);
        }).collect(Collectors.toList());
    }

    public int addFund(FundRequestResponse fundRequest) {
        CurrencyResponse currency = new CurrencyResponse();
        Fund fund = FundMapper.mapFromFundRequestToFund(fundRequest, getCurrencyById(currency.getCurrencyIdByCode(fundRequest.getCurrencyCode())));
        return repository.addFund(fund);
    }

    public int deleteFund(int id) {
        return repository.deleteFund(id);
    }

    public int reactivateFund(int id) {
        return repository.reactivateFund(id);
    }

}
