package com.alvaro.tfg.fundservice.mappers;

import com.alvaro.tfg.fundservice.api.CurrencyResponse;
import com.alvaro.tfg.fundservice.api.FundRequestResponse;
import com.alvaro.tfg.fundservice.model.Fund;

public class FundMapper {

    public static FundRequestResponse mapFromFundToFundResponse(Fund fund, CurrencyResponse currency) {
        FundRequestResponse fundResponse = new FundRequestResponse();
        fundResponse.setName(fund.getName());
        fundResponse.setRefNumber(fund.getRefNumber());
        fundResponse.setCurrencyCode(currency.getCode());
        fundResponse.setActive(fund.isActive());
        fundResponse.setActiveDate(fund.getActiveDate());
        fundResponse.setInactiveDate(fund.getInactiveDate());
        return fundResponse;
    }

    public static Fund mapFromFundRequestToFund(FundRequestResponse fundRequest, CurrencyResponse currency) {
        Fund fund = new Fund();
        fund.setName(fundRequest.getName());
        fund.setRefNumber(fundRequest.getRefNumber());
        fund.setCurrencyId(currency.getCurrencyIdByCode(currency.getCode()));
        return fund;
    }

}
