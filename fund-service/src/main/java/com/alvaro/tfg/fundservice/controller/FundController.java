package com.alvaro.tfg.fundservice.controller;

import com.alvaro.tfg.fundservice.api.CurrencyResponse;
import com.alvaro.tfg.fundservice.api.FundRequestResponse;
import com.alvaro.tfg.fundservice.services.FundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/funds")
public class FundController {

    @Autowired
    private FundService service;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllFunds() {
        try {
            return new ResponseEntity<>(service.getAllFunds(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Object> getFundById(@PathVariable int id) {
        try {
            return new ResponseEntity<>(service.getFundById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Fund with id " + id + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<Object> getFundByName(@PathVariable String name) {
        try {
            return new ResponseEntity<>(service.getFundByName(name), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Fund with name " + name + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getFundId/{name}")
    public ResponseEntity<Object> getFundIdByName(@PathVariable String name) {
        try {
            return new ResponseEntity<>(service.getFundIdByName(name), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(0, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/active")
    public ResponseEntity<Object> getActiveFunds() {
        try {
            return new ResponseEntity<>(service.getActiveFunds(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/inactive")
    public ResponseEntity<Object> getInactiveFunds() {
        try {
            return new ResponseEntity<>(service.getInactiveFunds(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<Object> addFund(@RequestBody FundRequestResponse fundRequest) {
        try {
            if (service.addFund(fundRequest) == 1) {
                return new ResponseEntity<>("Fund added successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Fund not added", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteFund(@PathVariable int id) {
        try {
            if (service.deleteFund(id) == 1) {
                return new ResponseEntity<>("Fund deleted successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Fund not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/reactivate/{id}")
    public ResponseEntity<Object> reactivateFund(@PathVariable int id) {
        try {
            if (service.reactivateFund(id) == 1) {
                return new ResponseEntity<>("Fund reactivated successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Fund not found", HttpStatus.NOT_FOUND);
        } catch (Exception e){
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
