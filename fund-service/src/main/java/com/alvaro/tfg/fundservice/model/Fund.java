package com.alvaro.tfg.fundservice.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Fund {

    private int id;

    private String name;

    private String refNumber;

    private int currencyId;

    private boolean active;

    private LocalDateTime activeDate;

    private LocalDateTime inactiveDate;


}
