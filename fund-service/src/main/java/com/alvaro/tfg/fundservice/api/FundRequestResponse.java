package com.alvaro.tfg.fundservice.api;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class FundRequestResponse {

    private String name;

    private String refNumber;

    private String currencyCode;

    private boolean active;

    private LocalDateTime activeDate;

    private LocalDateTime inactiveDate;

}
