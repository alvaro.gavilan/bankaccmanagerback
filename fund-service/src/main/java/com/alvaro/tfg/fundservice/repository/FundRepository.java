package com.alvaro.tfg.fundservice.repository;

import com.alvaro.tfg.fundservice.model.Fund;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FundRepository implements IFundRepository{

    @Autowired
    private JdbcTemplate jdbc;

    @Autowired
    private NamedParameterJdbcTemplate namedJdbc;

    public List<Fund> getAllFunds() {
        String query = "SELECT * FROM FUND";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Fund.class));
    }

    public Fund getFundById(int id) {
        String query = "SELECT * FROM FUND WHERE ID = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Fund.class), id);
    }

    public Fund getFundByName(String name) {
        String query = "SELECT * FROM FUND WHERE NAME = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Fund.class), name);
    }

    public List<Fund> getActiveFunds() {
        String query = "SELECT * FROM FUND WHERE ACTIVE = 1";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Fund.class));
    }

    public List<Fund> getInactiveFunds() {
        String query = "SELECT * FROM FUND WHERE ACTIVE = 0";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Fund.class));
    }

    public int addFund(Fund fund) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", fund.getName())
                .addValue("refNumber", fund.getRefNumber())
                .addValue("currencyId", fund.getCurrencyId());
        String query = "INSERT INTO FUND (NAME, REF_NUMBER, CURRENCY_ID, ACTIVE_DATE) values(:name, :refNumber, :currencyId, CURRENT_DATE())";
        return namedJdbc.update(query, params);
    }

    public int deleteFund(int id) {
        String query = "UPDATE FUND SET ACTIVE = 0, INACTIVE_DATE = CURRENT_DATE() WHERE ID = ?";
        return jdbc.update(query, id);
    }

    public int reactivateFund(int id) {
        String query = "UPDATE FUND SET ACTIVE = 1, ACTIVE_DATE = CURRENT_DATE, INACTIVE_DATE = NULL WHERE ID = ?";
        return jdbc.update(query, id);
    }

}
