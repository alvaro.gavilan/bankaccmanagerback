package com.alvaro.tfg.accountservice.mappers;

import com.alvaro.tfg.accountservice.api.AccountRequestResponse;
import com.alvaro.tfg.accountservice.api.ClientResponse;
import com.alvaro.tfg.accountservice.api.CurrencyResponse;
import com.alvaro.tfg.accountservice.model.Account;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

public class AccountMapper {

    public static AccountRequestResponse mapFromAccountToAccountResponse(Account account, ClientResponse client, CurrencyResponse currency) {
        AccountRequestResponse accountResponse = new AccountRequestResponse();
        accountResponse.setIban(account.getIban());
        accountResponse.setBalance(account.getBalance());
        accountResponse.setActive(account.isActive());
        accountResponse.setTitular(client.getName() + " " + client.getLastName1());
        accountResponse.setTitularIdentifier(client.getIdentifier());
        accountResponse.setCurrencyCode(currency.getCode());
        return accountResponse;
    }

    public static Account mapFromAccountRequestToAccount(AccountRequestResponse accountRequest) throws HttpClientErrorException {
        Account account = new Account();
       // try {
            ClientResponse client = new ClientResponse();
            CurrencyResponse currency = new CurrencyResponse();
            account.setIban(accountRequest.getIban());
            account.setBalance(accountRequest.getBalance());
            account.setClientId(client.getClientIdByIdentifier(accountRequest.getTitularIdentifier()));
            account.setCurrencyId(currency.getCurrencyIdByCode(accountRequest.getCurrencyCode()));
        /*} catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }*/

        return account;
    }

}
