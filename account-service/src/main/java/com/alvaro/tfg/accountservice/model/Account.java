package com.alvaro.tfg.accountservice.model;

import lombok.Data;

@Data
public class Account {

    private int id;

    private String iban;

    private double balance;

    private boolean active;

    private int clientId;

    private int currencyId;

}
