package com.alvaro.tfg.accountservice.repository;

import com.alvaro.tfg.accountservice.model.Account;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAccountRepository {

    public List<Account> getAllAccounts();

    public Account getAccountById(int id);

    public Account getAccountByIban(String iban);

    public List<Account> getActiveAccounts();

    public List<Account> getInactiveAccounts();

    public List<Account> getAccountsByClientId(int clientId);

    public int addAccount(Account account);

    public int deleteAccount(int id);

    public int reactivateAccount(int id);

}
