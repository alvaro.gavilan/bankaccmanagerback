package com.alvaro.tfg.accountservice.api;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Data
public class ClientResponse {

    private String name;

    private String lastName1;

    private String lastName2;

    private String email;

    private String phone;

    private String identifier;

    public int getClientIdByIdentifier(String identifier) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8091/clients/getId/";
        try {
            return restTemplate.getForObject(url + identifier, Integer.class);
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Client not found");
        }
    }

}
