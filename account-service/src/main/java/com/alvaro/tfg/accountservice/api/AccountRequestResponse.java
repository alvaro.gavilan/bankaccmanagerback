package com.alvaro.tfg.accountservice.api;

import lombok.Data;

@Data
public class AccountRequestResponse {

    private String iban;

    private double balance;

    private boolean active;

    private String titular;

    private String titularIdentifier;

    private String currencyCode;

}
