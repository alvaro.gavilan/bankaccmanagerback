package com.alvaro.tfg.accountservice.controller;

import com.alvaro.tfg.accountservice.api.AccountRequestResponse;
import com.alvaro.tfg.accountservice.model.Account;
import com.alvaro.tfg.accountservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    AccountService service;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllAccounts() {
        try {
            return new ResponseEntity<>(service.getAllAccounts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Object> getAccountById(@PathVariable int id) {
        try {
            return new ResponseEntity<>(service.getAccountById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Client with id " + id + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getAccountId/{iban}")
    public ResponseEntity<Object> getAccountIdByIban(@PathVariable String iban) {
        try {
            return new ResponseEntity<>(service.getAccountIdByIban(iban), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(0, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/iban/{iban}")
    public ResponseEntity<Object> getAccountByIban(@PathVariable String iban) {
        try {
            return new ResponseEntity<>(service.getAccountByIban(iban), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Client with iban " + iban + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/active")
    public ResponseEntity<Object> getActiveAccounts() {
        try {
            return new ResponseEntity<>(service.getActiveAccounts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/inactive")
    public ResponseEntity<Object> getInactiveAccounts() {
        try {
            return new ResponseEntity<>(service.getInactiveAccounts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/clientIdentifier/{clientIdentifier}")
    public ResponseEntity<Object> getAccountsByClientId(@PathVariable String clientIdentifier) {
        try {
            return new ResponseEntity<>(service.getAccountsByClientIdentifier(clientIdentifier), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<Object> addAccount(@RequestBody AccountRequestResponse accountRequest) {
        try {
            if (service.addAccount(accountRequest) == 1) {
                return new ResponseEntity<>("Account created successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Account not added", HttpStatus.BAD_REQUEST);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteAccount(@PathVariable int id) {
        try {
            if (service.deleteAccount(id) == 1) {
                return new ResponseEntity<>("Account deleted successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Account not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/reactivate/{id}")
    public ResponseEntity<Object> reactivateAccount(@PathVariable int id) {
        try {
            if (service.reactivateAccount(id) == 1) {
                return new ResponseEntity<>("Account reactivated successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Account not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
