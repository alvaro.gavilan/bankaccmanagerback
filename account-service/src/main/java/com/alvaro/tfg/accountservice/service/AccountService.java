package com.alvaro.tfg.accountservice.service;

import com.alvaro.tfg.accountservice.api.AccountRequestResponse;
import com.alvaro.tfg.accountservice.api.ClientResponse;
import com.alvaro.tfg.accountservice.api.CurrencyResponse;
import com.alvaro.tfg.accountservice.mappers.AccountMapper;
import com.alvaro.tfg.accountservice.model.Account;
import com.alvaro.tfg.accountservice.repository.IAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountService {

    @Autowired
    private IAccountRepository repository;

    private final RestTemplate REST_TEMPLATE = new RestTemplate();

    public List<Account> getAllAccounts() {
        return repository.getAllAccounts();
    }

    public CurrencyResponse getCurrencyById(int currencyId) {
        String url = "http://localhost:8092/currencies/id/";
        return REST_TEMPLATE.getForObject(url + currencyId, CurrencyResponse.class);
    }

    public ClientResponse getClientById(int clientId) {
        String url = "http://localhost:8091/clients/id/";
        return REST_TEMPLATE.getForObject(url + clientId, ClientResponse.class);
    }

    public AccountRequestResponse getAccountById(int id) {
        Account account = repository.getAccountById(id);
        ClientResponse client = getClientById(account.getClientId());
        CurrencyResponse currency = getCurrencyById(account.getCurrencyId());
        return AccountMapper.mapFromAccountToAccountResponse(account, client, currency);
    }

    public int getAccountIdByIban(String iban) {
        Account account = repository.getAccountByIban(iban);
        return account.getId();
    }

    public AccountRequestResponse getAccountByIban(String iban) {
        Account account = repository.getAccountByIban(iban);
        ClientResponse client = getClientById(account.getClientId());
        CurrencyResponse currency = getCurrencyById(account.getCurrencyId());
        return AccountMapper.mapFromAccountToAccountResponse(account, client, currency);
    }

    public List<AccountRequestResponse> getActiveAccounts() {
        List<Account> accounts = repository.getActiveAccounts();
        return accounts.stream().map(account -> {
            ClientResponse client = getClientById(account.getClientId());
            CurrencyResponse currency = getCurrencyById(account.getCurrencyId());
            return AccountMapper.mapFromAccountToAccountResponse(account, client, currency);
        }).collect(Collectors.toList());
    }

    public List<AccountRequestResponse> getInactiveAccounts() {
        List<Account> accounts = repository.getInactiveAccounts();
        return accounts.stream().map(account -> {
            ClientResponse client = getClientById(account.getClientId());
            CurrencyResponse currency = getCurrencyById(account.getCurrencyId());
            return AccountMapper.mapFromAccountToAccountResponse(account, client, currency);
        }).collect(Collectors.toList());
    }

    public List<AccountRequestResponse> getAccountsByClientIdentifier(String clientIdentifier) {
        ClientResponse clientResponse = new ClientResponse();
        List<Account> accounts = repository.getAccountsByClientId(clientResponse.getClientIdByIdentifier(clientIdentifier));
        return accounts.stream().map(account -> {
            ClientResponse client = getClientById(account.getClientId());
            CurrencyResponse currency = getCurrencyById(account.getCurrencyId());
            return AccountMapper.mapFromAccountToAccountResponse(account, client, currency);
        }).collect(Collectors.toList());
    }

    public int addAccount(AccountRequestResponse accountRequest) {
        Account account = AccountMapper.mapFromAccountRequestToAccount(accountRequest);
        return repository.addAccount(account);
    }

    public int deleteAccount(int id) {
        return repository.deleteAccount(id);
    }

    public int reactivateAccount(int id) {
        return repository.reactivateAccount(id);
    }

}
