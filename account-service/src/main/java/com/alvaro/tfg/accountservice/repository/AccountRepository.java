package com.alvaro.tfg.accountservice.repository;

import com.alvaro.tfg.accountservice.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AccountRepository implements IAccountRepository{

    @Autowired
    private JdbcTemplate jdbc;

    @Autowired
    private NamedParameterJdbcTemplate namedJdbc;

    public List<Account> getAllAccounts() {
        String query = "SELECT * FROM ACCOUNT";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Account.class));
    }

    public Account getAccountById(int id) {
        String query = "SELECT * FROM ACCOUNT WHERE ID = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Account.class), id);
    }

    public Account getAccountByIban(String iban) {
        String query = "SELECT * FROM ACCOUNT WHERE IBAN = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Account.class), iban);
    }

    public List<Account> getActiveAccounts() {
        String query = "SELECT * FROM ACCOUNT WHERE ACTIVE = 1";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Account.class));
    }

    public List<Account> getInactiveAccounts() {
        String query = "SELECT * FROM ACCOUNT WHERE ACTIVE = 0";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Account.class));
    }

    public List<Account> getAccountsByClientId(int clientId) {
        String query = "SELECT * FROM ACCOUNT WHERE CLIENT_ID = ?";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Account.class), clientId);
    }

    public int addAccount(Account account) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("iban", account.getIban())
                .addValue("balance", account.getBalance())
                .addValue("clientId", account.getClientId())
                .addValue("currencyId", account.getCurrencyId());
        String query = "INSERT INTO ACCOUNT (IBAN, BALANCE, CLIENT_ID, CURRENCY_ID) " +
                "VALUES (:iban, :balance, :clientId, :currencyId)";
        return namedJdbc.update(query, params);
    }

    public int deleteAccount(int id) {
        String query = "UPDATE ACCOUNT SET ACTIVE = 0 WHERE ID = ?";
        return jdbc.update(query, id);
    }

    public int reactivateAccount(int id) {
        String query = "UPDATE ACCOUNT SET ACTIVE = 1 WHERE ID = ?";
        return jdbc.update(query, id);
    }

}
