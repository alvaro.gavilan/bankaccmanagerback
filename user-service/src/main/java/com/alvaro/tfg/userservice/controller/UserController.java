package com.alvaro.tfg.userservice.controller;

import com.alvaro.tfg.userservice.api.UserRequestResponse;
import com.alvaro.tfg.userservice.model.User;
import com.alvaro.tfg.userservice.services.UserSercice;
import com.alvaro.tfg.userservice.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    UserSercice service;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllUsers() {
        try{
            return new ResponseEntity<>(service.getAllUsers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/active")
    public ResponseEntity<Object> getActiveUsers() {
        try {
            return new ResponseEntity<>(service.getActiveUsers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/inactive")
    public ResponseEntity<Object> getInactiveUsers() {
        try {
            return new ResponseEntity<>(service.getInactiveUsers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable int id) {
        try {
            return new ResponseEntity<>(service.getUserById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<Object> getUserByEmail(@PathVariable String email) {
        try {
            return new ResponseEntity<>(service.getUserByEmail(email), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<String> addUser(@RequestBody UserRequestResponse userRequest) {
        if (UserUtils.isValidEmail(userRequest.getEmail())) {
            if (UserUtils.isValidPhone(userRequest.getPhone())) {
                try {
                    if (service.addUser(userRequest) == 1) {
                        return new ResponseEntity<>("User added successfully", HttpStatus.CREATED);
                    }
                    return new ResponseEntity<>("User not added", HttpStatus.BAD_REQUEST);
                } catch (Exception e) {
                    return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            return new ResponseEntity<>("Wrong phone format", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Wrong email format", HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateUser(@RequestBody UserRequestResponse userRequest, @PathVariable int id) {
        if (UserUtils.isValidEmail(userRequest.getEmail())) {
            if (UserUtils.isValidPhone(userRequest.getPhone())) {
                try {
                    if (service.updateUser(userRequest, id) == 1){
                        return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
                    }
                    return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
                } catch (Exception e) {
                    return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            return new ResponseEntity<>("Wrong phone format", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Wrong email format", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable int id) {
        try {
            if (service.deleteUser(id) == 1) {
                return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/reactivate/{id}")
    public ResponseEntity<String> reactivateUser(@PathVariable int id) {
        try {
            if (service.reactivateUser(id) == 1) {
                return new ResponseEntity<>("User reactivated successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
