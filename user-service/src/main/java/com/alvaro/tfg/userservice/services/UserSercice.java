package com.alvaro.tfg.userservice.services;

import com.alvaro.tfg.userservice.api.UserRequestResponse;
import com.alvaro.tfg.userservice.mappers.UserMapper;
import com.alvaro.tfg.userservice.model.User;
import com.alvaro.tfg.userservice.repository.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserSercice {

    @Autowired
    IUserRepository repository;

    public List<User> getAllUsers() {
        return repository.getAllUsers();
    }

    public List<User> getActiveUsers() {
        return repository.getActiveUsers();
    }

    public List<User> getInactiveUsers() {
        return repository.getInactiveUsers();
    }

    public UserRequestResponse getUserById(int id) {
        User user = repository.getUserById(id);
        return UserMapper.mapFromUserToUserResponse(user);
    }

    public UserRequestResponse getUserByEmail(String email) {
        User user = repository.getUserByEmail(email);
        return UserMapper.mapFromUserToUserResponse(user);
    }

    public int addUser(UserRequestResponse userRequest) {
        User user = UserMapper.mapFromUserRequestToUser(userRequest);
        return repository.createUser(user);
    }

    public int updateUser(UserRequestResponse userRequest, int id) {
        User user = UserMapper.mapFromUserRequestToUser(userRequest);
        return repository.updateUser(user, id);
    }

    public int deleteUser(int id) {
        return repository.deleteUser(id);
    }

    public int reactivateUser(int id) {
        return repository.reactivateUser(id);
    }

}
