package com.alvaro.tfg.userservice.mappers;

import com.alvaro.tfg.userservice.api.UserRequestResponse;
import com.alvaro.tfg.userservice.model.User;

public class UserMapper {

    public static UserRequestResponse mapFromUserToUserResponse(User user) {
        UserRequestResponse userRequestResponse = new UserRequestResponse();
        userRequestResponse.setName(user.getName());
        userRequestResponse.setLastName1(user.getLastName1());
        userRequestResponse.setLastName2(user.getLastName2());
        userRequestResponse.setEmail(user.getEmail());
        userRequestResponse.setPhone(user.getPhone());
        return userRequestResponse;
    }

    public static User mapFromUserRequestToUser(UserRequestResponse userRequest) {
        User user = new User();
        user.setName(userRequest.getName());
        user.setLastName1(userRequest.getLastName1());
        user.setLastName2(userRequest.getLastName2());
        user.setEmail(userRequest.getEmail());
        user.setPhone(userRequest.getPhone());
        return user;
    }

}
