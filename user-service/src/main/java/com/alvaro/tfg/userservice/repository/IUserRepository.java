package com.alvaro.tfg.userservice.repository;

import com.alvaro.tfg.userservice.model.User;

import java.util.List;

public interface IUserRepository {

    public List<User> getAllUsers();

    public List<User> getActiveUsers();

    public List<User> getInactiveUsers();

    public User getUserById(int id);

    public User getUserByEmail(String email);

    public int createUser(User user);

    public int updateUser(User updatedUser, int id);

    public int deleteUser(int id);

    public int reactivateUser(int id);

}
