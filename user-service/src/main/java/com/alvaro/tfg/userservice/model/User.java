package com.alvaro.tfg.userservice.model;

import lombok.Data;

@Data
public class User {

    private int id;

    private String name;

    private String lastName1;

    private String lastName2;

    private String email;

    private String phone;

    private boolean active;

}
