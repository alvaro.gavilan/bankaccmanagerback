package com.alvaro.tfg.userservice.repository;

import com.alvaro.tfg.userservice.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository implements IUserRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    NamedParameterJdbcTemplate namedJdbc;

    public List<User> getAllUsers() {
        String query = "SELECT * FROM USER";
        return jdbcTemplate.query(query, new BeanPropertyRowMapper<>(User.class));
    }

    public List<User> getActiveUsers() {
        String query = "SELECT * FROM USER WHERE ACTIVE = 1";
        return jdbcTemplate.query(query, new BeanPropertyRowMapper<>(User.class));
    }

    public List<User> getInactiveUsers() {
        String query = "SELECT * FROM USER WHERE ACTIVE = 0";
        return jdbcTemplate.query(query, new BeanPropertyRowMapper<>(User.class));
    }

    public User getUserById(int id) {
        String query = "SELECT * FROM USER WHERE ID = ?";
        return jdbcTemplate.queryForObject(query, new BeanPropertyRowMapper<>(User.class), id);
    }

    public User getUserByEmail(String email) {
        String query = "SELECT * FROM USER WHERE EMAIL = ?";
        return jdbcTemplate.queryForObject(query, new BeanPropertyRowMapper<>(User.class), email);
    }

    public int createUser(User user) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", user.getName())
                .addValue("lastName1", user.getLastName1())
                .addValue("lastName2", user.getLastName2())
                .addValue("email", user.getEmail())
                .addValue("phone", user.getPhone())
                ;
        String query = "INSERT INTO USER (name, last_name1, last_name2, email, phone)" +
                "VALUES(:name, :lastName1, :lastName2, :email, :phone)";
        return namedJdbc.update(query, params);
    }

    public int updateUser(User user, int id) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", user.getName())
                .addValue("lastName1", user.getLastName1())
                .addValue("lastName2", user.getLastName2())
                .addValue("email", user.getEmail())
                .addValue("phone", user.getPhone())
                .addValue("id", id);
        String query = "UPDATE USER SET " +
                "NAME = :name, " +
                "LAST_NAME1 = :lastName1, " +
                "LAST_NAME2 = :lastName2, " +
                "EMAIL = :email, " +
                "PHONE = :phone " +
                "WHERE ID = :id";
        return namedJdbc.update(query, params);
    }

    public int deleteUser(int id){
        String query = "UPDATE USER SET ACTIVE = 0 WHERE ID = ?";
        return jdbcTemplate.update(query, id);
    }

    public int reactivateUser(int id) {
        String query = "UPDATE USER SET ACTIVE = 1 WHERE ID = ?";
        return jdbcTemplate.update(query, id);
    }

}
