package com.alvaro.tfg.userservice.api;

import lombok.Data;

@Data
public class UserRequestResponse {

    private String name;

    private String lastName1;

    private String lastName2;

    private String email;

    private String phone;

}
