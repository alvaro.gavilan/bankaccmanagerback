package com.alvaro.tfg.userservice.utils;

import java.util.regex.Pattern;

public class UserUtils {

    private static final String EMAIL_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";

    private static final String PHONE_REGEX = "^\\+?\\d{1,3}[-.\\s]?\\(?\\d{1,3}\\)?[-.\\s]?\\d{1,4}[-.\\s]?\\d{1,4}$";
    public static boolean isValidEmail(String email) {
        if (email.equals("")) return false;
        return Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE).matcher(email).find();
    }

    public static boolean isValidPhone(String phone) {
        if (phone.equals("")) return false;
        return Pattern.compile(PHONE_REGEX, Pattern.CASE_INSENSITIVE).matcher(phone).find();
    }

}
