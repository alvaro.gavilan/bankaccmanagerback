package com.alvaro.tfg.productservice.service;

import com.alvaro.tfg.productservice.api.ProductRequestResponse;
import com.alvaro.tfg.productservice.mappers.ProductMapper;
import com.alvaro.tfg.productservice.model.Product;
import com.alvaro.tfg.productservice.repository.IProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    IProductRepository repository;

    public List<Product> getAllProducts() {
        return repository.getAllProducts();
    }

    public ProductRequestResponse getProductById(int id) {
        return ProductMapper.mapFromProductToProductResponse(repository.getProductById(id));
    }

    public ProductRequestResponse getProductByName(String name) {
        return ProductMapper.mapFromProductToProductResponse(repository.getProductByName(name));
    }

    public List<ProductRequestResponse> getActiveProducts() {
        List<Product> products = repository.getActiveProducts();
        return products.stream().map(product -> ProductMapper.mapFromProductToProductResponse(product)).collect(Collectors.toList());
    }

    public List<ProductRequestResponse> getInactiveProducts() {
        List<Product> products = repository.getInactiveProducts();
        return products.stream().map(product -> ProductMapper.mapFromProductToProductResponse(product)).collect(Collectors.toList());
    }

    public int addProduct(String name) {
        return repository.addProduct(name);
    }

    public int deleteProduct(int id) {
        return repository.deleteProduct(id);
    }

    public int reactivateProduct(int id) {
        return repository.reactivateProduct(id);
    }

}
