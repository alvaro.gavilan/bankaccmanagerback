package com.alvaro.tfg.productservice.api;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ProductRequestResponse {

    private String name;

    private boolean active;

    private LocalDateTime activeDate;

    private LocalDateTime inactiveDate;


}
