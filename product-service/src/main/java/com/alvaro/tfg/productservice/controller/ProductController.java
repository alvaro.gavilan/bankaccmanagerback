package com.alvaro.tfg.productservice.controller;

import com.alvaro.tfg.productservice.api.ProductRequestResponse;
import com.alvaro.tfg.productservice.model.Product;
import com.alvaro.tfg.productservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductService service;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllProducts() {
        try {
            return new ResponseEntity<>(service.getAllProducts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable int id) {
        try {
            return new ResponseEntity<>(service.getProductById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Product with id " + id + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<Object> getProductByName(@PathVariable String name) {
        try {
            return new ResponseEntity<>(service.getProductByName(name), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Product with name " + name + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/active")
    public ResponseEntity<Object> getActiveProducts() {
        try {
            return new ResponseEntity<>(service.getActiveProducts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/inactive")
    public ResponseEntity<Object> getInactiveProducts() {
        try {
            return new ResponseEntity<>(service.getInactiveProducts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<Object> addProduct(@RequestBody String name) {
        try {
            if (service.addProduct(name) == 1) {
                return new ResponseEntity<>("Product added successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable int id) {
        try {
            if (service.deleteProduct(id) == 1) {
                return new ResponseEntity<>("Product deleted successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/reactivate/{id}")
    public ResponseEntity<Object> reactivateProduct(@PathVariable int id) {
        try {
            if (service.reactivateProduct(id) == 1) {
                return new ResponseEntity<>("Product reactivated successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Product not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
