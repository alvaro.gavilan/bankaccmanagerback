package com.alvaro.tfg.productservice.mappers;

import com.alvaro.tfg.productservice.api.ProductRequestResponse;
import com.alvaro.tfg.productservice.model.Product;

public class ProductMapper {

    public static ProductRequestResponse mapFromProductToProductResponse(Product product) {
        ProductRequestResponse productResponse = new ProductRequestResponse();
        productResponse.setName(product.getName());
        productResponse.setActive(product.isActive());
        productResponse.setActiveDate(product.getActiveDate());
        productResponse.setInactiveDate(product.getInactiveDate());
        return productResponse;
    }

    public static Product mapFromProductRequestToProduct(ProductRequestResponse productRequest) {
        Product product = new Product();
        product.setName(productRequest.getName());
        product.setActive(productRequest.isActive());
        product.setActiveDate(productRequest.getActiveDate());
        product.setInactiveDate(productRequest.getInactiveDate());
        return product;
    }

}
