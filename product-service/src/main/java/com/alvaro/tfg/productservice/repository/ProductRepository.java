package com.alvaro.tfg.productservice.repository;

import com.alvaro.tfg.productservice.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductRepository implements IProductRepository{

    @Autowired
    private JdbcTemplate jdbc;

    @Autowired
    private NamedParameterJdbcTemplate namedJdbc;

    public List<Product> getAllProducts() {
        String query = "SELECT * FROM PRODUCT";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Product.class));
    }

    public Product getProductById(int id) {
        String query = "SELECT * FROM PRODUCT WHERE ID = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Product.class), id);
    }

    public Product getProductByName(String name) {
        String query = "SELECT * FROM PRODUCT WHERE NAME = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Product.class),name);
    }

    public List<Product> getActiveProducts() {
        String query = "SELECT * FROM PRODUCT WHERE ACTIVE = 1";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Product.class));
    }

    public List<Product> getInactiveProducts() {
        String query = "SELECT * FROM PRODUCT WHERE ACTIVE = 0";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Product.class));
    }

    public int addProduct(String name) {
        String query = "INSERT INTO PRODUCT (NAME, ACTIVE_DATE) values(?, CURRENT_DATE())";
        return jdbc.update(query, name);
    }

    public int deleteProduct(int id) {
        String query = "UPDATE PRODUCT SET ACTIVE = 0, INACTIVE_DATE = CURRENT_DATE() WHERE ID = ?";
        return jdbc.update(query, id);
    }

    public int reactivateProduct(int id) {
        String query = "UPDATE PRODUCT SET ACTIVE = 1, ACTIVE_DATE = CURRENT_DATE(), INACTIVE_DATE = NULL WHERE ID = ?";
        return jdbc.update(query, id);
    }

}
