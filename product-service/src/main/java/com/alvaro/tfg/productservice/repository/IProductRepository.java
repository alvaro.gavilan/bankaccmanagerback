package com.alvaro.tfg.productservice.repository;

import com.alvaro.tfg.productservice.model.Product;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IProductRepository {

    public List<Product> getAllProducts();

    public Product getProductById(int id);

    public Product getProductByName(String name);

    public List<Product> getActiveProducts();

    public List<Product> getInactiveProducts();

    public int addProduct(String name);

    public int deleteProduct(int id);

    public int reactivateProduct(int id);

}
