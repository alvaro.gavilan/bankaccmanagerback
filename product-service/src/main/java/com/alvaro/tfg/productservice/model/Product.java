package com.alvaro.tfg.productservice.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Product {

    private int id;

    private String name;

    private boolean active;

    private LocalDateTime activeDate;

    private LocalDateTime inactiveDate;

}
