package com.alvaro.tfg.clientservice.controller;

import com.alvaro.tfg.clientservice.api.ClientRequestResponse;
import com.alvaro.tfg.clientservice.service.ClientService;
import com.alvaro.tfg.clientservice.utils.ClientUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clients")
public class ClientController {

    @Autowired
    ClientService service;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllClients() {
        try {
            return new ResponseEntity<>(service.getAllClients(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/active")
    public ResponseEntity<Object> getActiveClients() {
        try {
            return new ResponseEntity<>(service.getActiveClients(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/inactive")
    public ResponseEntity<Object> getInactiveClients() {
        try {
            return new ResponseEntity<>(service.getInactiveClients(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Object> getClientById(@PathVariable int id) {
        try {
            return new ResponseEntity<>(service.getClientById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<Object> getClientByEmail(@PathVariable String email) {
        try {
            return new ResponseEntity<>(service.getClientByEmail(email), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/identifier/{identifier}")
    public ResponseEntity<Object> getClientByIdentifier(@PathVariable String identifier) {
        try {
            return new ResponseEntity<>(service.getClientByIdentifier(identifier), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getId/{identifier}")
    public ResponseEntity<Object> getClientId(@PathVariable String identifier) {
        try {
            return new ResponseEntity<>(service.getClientId(identifier), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<String> addClient(@RequestBody ClientRequestResponse clientRequest) {
        if (ClientUtils.isValidEmail(clientRequest.getEmail())) {
            if (ClientUtils.isValidPhone(clientRequest.getPhone())) {
                try {
                    if (service.addClient(clientRequest) == 1){
                        return new ResponseEntity<>("Client added successfully", HttpStatus.OK);
                    }
                    return new ResponseEntity<>("Client not added", HttpStatus.BAD_REQUEST);
                } catch (Exception e) {
                    return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            return new ResponseEntity<>("Wrong phone format", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Wrong email format", HttpStatus.BAD_REQUEST);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateClient(@RequestBody ClientRequestResponse clientRequest, @PathVariable int id) {
        if (ClientUtils.isValidEmail(clientRequest.getEmail())) {
            if (ClientUtils.isValidPhone(clientRequest.getPhone())) {
                try {
                    if (service.updateClient(clientRequest, id) == 1){
                        return new ResponseEntity<>("Client updated successfully", HttpStatus.OK);
                    }
                    return new ResponseEntity<>("Client not updated", HttpStatus.BAD_REQUEST);
                } catch (Exception e) {
                    return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            return new ResponseEntity<>("Wrong phone format", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Wrong email format", HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteClient(@PathVariable int id) {
        try {
            if (service.deleteClient(id) == 1) {
                return new ResponseEntity<>("Client deleted successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/reactivate/{id}")
    public ResponseEntity<String> reactivateClient(@PathVariable int id) {
        try {
            if (service.reactivateClient(id) == 1) {
                return new ResponseEntity<>("Client reactivated successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PatchMapping("/changeUserId/{id}/{newUserId}")
    public  ResponseEntity<String> changeClientUserId(@PathVariable int id, @PathVariable int newUserId) {
        try {
            if (service.changeClientUserId(id, newUserId) == 1){
                return new ResponseEntity<>("Client userID changed successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
