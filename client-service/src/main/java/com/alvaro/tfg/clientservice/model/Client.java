package com.alvaro.tfg.clientservice.model;

import lombok.Data;

@Data
public class Client {

    private int id;

    private String name;

    private String lastName1;

    private String lastName2;

    private String email;

    private String phone;

    private String identifier;

    private boolean active;

    private int userId;

}
