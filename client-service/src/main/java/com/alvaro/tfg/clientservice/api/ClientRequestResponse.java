package com.alvaro.tfg.clientservice.api;

import lombok.Data;

@Data
public class ClientRequestResponse {

    private String name;

    private String lastName1;

    private String lastName2;

    private String email;

    private String phone;

    private String identifier;

}
