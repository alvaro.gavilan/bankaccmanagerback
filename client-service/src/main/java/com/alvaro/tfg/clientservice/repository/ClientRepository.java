package com.alvaro.tfg.clientservice.repository;

import com.alvaro.tfg.clientservice.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClientRepository implements IClientRepository{

    @Autowired
    JdbcTemplate jdbc;

    @Autowired
    NamedParameterJdbcTemplate namedJdbc;

    public List<Client> getAllClients() {
        String query = "SELECT * FROM CLIENT";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Client.class));
    }

    public List<Client> getActiveClients() {
        String query = "SELECT * FROM CLIENT WHERE ACTIVE = 1";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Client.class));
    }

    public List<Client> getInactiveClients() {
        String query = "SELECT * FROM CLIENT WHERE ACTIVE = 0";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Client.class));
    }

    public Client getClientById(int id) {
        String query = "SELECT * FROM CLIENT WHERE ID = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Client.class), id);
    }

    public Client getClientByEmail(String email) {
        String query = "SELECT * FROM CLIENT WHERE EMAIL = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Client.class), email);
    }

    public Client getClientByIdentifier(String identifier) {
        String query = "SELECT * FROM CLIENT WHERE IDENTIFIER = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Client.class), identifier);
    }

    public int createClient(Client client) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", client.getName())
                .addValue("lastName1", client.getLastName1())
                .addValue("lastName2", client.getLastName2())
                .addValue("email", client.getEmail())
                .addValue("phone", client.getPhone())
                .addValue("identifier", client.getIdentifier());
        String query = "INSERT INTO CLIENT (NAME, LAST_NAME1, LAST_NAME2, EMAIL, PHONE, IDENTIFIER, USER_ID) " +
                "(SELECT :name, :lastName1, :lastName2, :email, :phone, :identifier, ID " +
                "FROM (SELECT id FROM `user` u order BY rand()) ids LIMIT 1)";
        return namedJdbc.update(query, params);
    }

//    INSERT INTO CLIENT (NAME, LAST_NAME1, LAST_NAME2, EMAIL, PHONE, IDENTIFIER, USER_ID)
//    (SELECT 'Juan', 'Perez', 'Rodriguez', 'PEPE.perez@example.com', '123456789', '6666666661', ID
//    FROM (SELECT id FROM `user` u order BY rand()) ids LIMIT 1);

    public int updateClient(Client client, int id) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", client.getName())
                .addValue("lastName1", client.getLastName1())
                .addValue("lastName2", client.getLastName2())
                .addValue("email", client.getEmail())
                .addValue("phone", client.getPhone())
                .addValue("identifier", client.getIdentifier())
                .addValue("id", id);
        String query = "UPDATE CLIENT SET " +
                "NAME = :name, " +
                "LAST_NAME1 = :lastName1, " +
                "LAST_NAME2 = :lastName2, " +
                "EMAIL = :email, " +
                "PHONE = :phone, " +
                "IDENTIFIER = :identifier " +
                "WHERE ID = :id";
        return namedJdbc.update(query, params);
    }

    public int deleteClient(int id) {
        String query = "UPDATE CLIENT SET ACTIVE = 0 WHERE ID = ?";
        return jdbc.update(query, id);
    }

    public int reactivateClient(int id) {
        String query = "UPDATE CLIENT SET ACTIVE = 1 WHERE ID = ?";
        return jdbc.update(query, id);
    }

    public int changeClientUserId(int id, int newUserId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("newUserId", newUserId)
                .addValue("id", id);
        String query = "UPDATE CLIENT SET USER_ID = :newUserId WHERE ID = :id";
        return namedJdbc.update(query, params);
    }

}
