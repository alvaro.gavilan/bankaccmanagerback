package com.alvaro.tfg.clientservice.service;

import com.alvaro.tfg.clientservice.api.ClientRequestResponse;
import com.alvaro.tfg.clientservice.mappers.ClientMapper;
import com.alvaro.tfg.clientservice.model.Client;
import com.alvaro.tfg.clientservice.repository.ClientRepository;
import com.alvaro.tfg.clientservice.repository.IClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    IClientRepository repository;

    public List<Client> getAllClients() {
        return repository.getAllClients();
    }

    public List<Client> getActiveClients() {
        return repository.getActiveClients();
    }

    public List<Client> getInactiveClients() {
        return repository.getInactiveClients();
    }

    public ClientRequestResponse getClientById(int id) {
        Client client = repository.getClientById(id);
        return ClientMapper.mapFromClientToClientResponse(client);
    }

    public ClientRequestResponse getClientByEmail(String email) {
        Client client = repository.getClientByEmail(email);
        return ClientMapper.mapFromClientToClientResponse(client);
    }

    public ClientRequestResponse getClientByIdentifier(String identifier) {
        Client client = repository.getClientByIdentifier(identifier);
        return ClientMapper.mapFromClientToClientResponse(client);
    }

    public int getClientId(String identifier) {
        Client client = repository.getClientByIdentifier(identifier);
        return client.getId();
    }

    public int addClient(ClientRequestResponse clientRequest) {
        Client client = ClientMapper.mapFromClientRequestToClient(clientRequest);
        return repository.createClient(client);
    }

    public int updateClient(ClientRequestResponse clientRequest, int id) {
        Client client = ClientMapper.mapFromClientRequestToClient(clientRequest);
        return repository.updateClient(client, id);
    }

    public int deleteClient(int id) {
        return repository.deleteClient(id);
    }

    public int reactivateClient(int id) {
        return repository.reactivateClient(id);
    }

    public int changeClientUserId(int id, int newUserId) {
        return repository.changeClientUserId(id, newUserId);
    }

}
