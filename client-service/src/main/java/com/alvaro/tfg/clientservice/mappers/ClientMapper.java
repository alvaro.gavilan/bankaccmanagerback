package com.alvaro.tfg.clientservice.mappers;

import com.alvaro.tfg.clientservice.api.ClientRequestResponse;
import com.alvaro.tfg.clientservice.model.Client;

public class ClientMapper {

    public static ClientRequestResponse mapFromClientToClientResponse(Client client) {
        ClientRequestResponse clientResponse = new ClientRequestResponse();
        clientResponse.setName(client.getName());
        clientResponse.setLastName1(client.getLastName1());
        clientResponse.setLastName2(client.getLastName2());
        clientResponse.setEmail(client.getEmail());
        clientResponse.setPhone(client.getPhone());
        clientResponse.setIdentifier(client.getIdentifier());
        return clientResponse;
    }

    public static Client mapFromClientRequestToClient(ClientRequestResponse clientRequest) {
        Client client = new Client();
        client.setName(clientRequest.getName());
        client.setLastName1(clientRequest.getLastName1());
        client.setLastName2(clientRequest.getLastName2());
        client.setEmail(clientRequest.getEmail());
        client.setPhone(clientRequest.getPhone());
        client.setIdentifier(clientRequest.getIdentifier());
        return client;
    }

}
