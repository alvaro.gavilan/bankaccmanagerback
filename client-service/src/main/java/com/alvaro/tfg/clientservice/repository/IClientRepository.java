package com.alvaro.tfg.clientservice.repository;

import com.alvaro.tfg.clientservice.model.Client;

import java.util.List;

public interface IClientRepository {

    public List<Client> getAllClients();

    public List<Client> getActiveClients();

    public List<Client> getInactiveClients();

    public Client getClientById(int id);

    public  Client getClientByEmail(String email);

    public Client getClientByIdentifier(String identifier);

    public int createClient(Client client);

    public int updateClient(Client client, int id);

    public int deleteClient(int id);

    public int reactivateClient(int id);

    public int changeClientUserId(int id, int newUserId);

}
