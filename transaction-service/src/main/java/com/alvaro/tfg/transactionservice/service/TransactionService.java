package com.alvaro.tfg.transactionservice.service;

import com.alvaro.tfg.transactionservice.api.AccountResponse;
import com.alvaro.tfg.transactionservice.api.FundResponse;
import com.alvaro.tfg.transactionservice.api.TransactionRequestResponse;
import com.alvaro.tfg.transactionservice.mappers.TransactionMapper;
import com.alvaro.tfg.transactionservice.model.Transaction;
import com.alvaro.tfg.transactionservice.repository.ITransactionRepository;
import com.alvaro.tfg.transactionservice.utils.TransactionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    @Autowired
    ITransactionRepository repository;

    @Autowired
    TransactionMapper transactionMapper;

    @Autowired
    TransactionUtils tUtils;

    private final RestTemplate REST_TEMPLATE = new RestTemplate();

    public List<Transaction> getAllTransactions() {
        return repository.getAllTransactions();
    }

    public FundResponse getFundById(int fundId) {
        String url = "http://localhost:8093/funds/id/";
        return REST_TEMPLATE.getForObject(url + fundId, FundResponse.class);
    }

    public AccountResponse getAccountById(int accId) {
        String url = "http://localhost:8095/accounts/id/";
        return REST_TEMPLATE.getForObject(url + accId, AccountResponse.class);
    }

    public List<TransactionRequestResponse> getAllTransactionsByDate(String date) {
        List<Transaction> transactions = repository.getAllTransactionsByDate(date);
        return mapTransactionList(transactions);
    }

    public List<TransactionRequestResponse> mapTransactionList(List<Transaction> transactions){
        return transactions.stream().map(transaction -> {
            FundResponse fund = getFundById(transaction.getFundId());
            AccountResponse account = getAccountById(transaction.getAccountId());
            return transactionMapper.mapFromTransactionToTransactionResponse(transaction, fund, account);
        }).collect(Collectors.toList());
    }

    public TransactionRequestResponse getTransactionById(int id) {
        Transaction transaction = repository.getTransactionById(id);
        FundResponse fund = getFundById(transaction.getFundId());
        AccountResponse account = getAccountById(transaction.getAccountId());
        return transactionMapper.mapFromTransactionToTransactionResponse(transaction, fund, account);
    }

    public TransactionRequestResponse getTransactionByTNumber(String tNumber) {
        Transaction transaction = repository.getTransactionByTNumber(tNumber);
        FundResponse fund = getFundById(transaction.getFundId());
        AccountResponse account = getAccountById(transaction.getAccountId());
        return transactionMapper.mapFromTransactionToTransactionResponse(transaction, fund, account);
    }

    public List<TransactionRequestResponse> getTransactionsLessThan(int amount) {
        List<Transaction> transactions = repository.getTransactionsLessThan(amount);
        return mapTransactionList(transactions);
    }

    public List<TransactionRequestResponse> getTransactionsMoreThan(int amount) {
        List<Transaction> transactions = repository.getTransactionsMoreThan(amount);
        return mapTransactionList(transactions);
    }

    public List<TransactionRequestResponse> getTransactionsByAccId(int id) {
        List<Transaction> transactions = repository.getTransactionsByAccId(id);
        return mapTransactionList(transactions);
    }

    public List<TransactionRequestResponse> getTransactionsByFundId(int id) {
        List<Transaction> transactions = repository.getTransactionsByFundId(id);
        return mapTransactionList(transactions);
    }

    public int addTransaction(TransactionRequestResponse transactionRequest) throws Exception {
        if (!tUtils.accountExists(transactionRequest.getIban())) throw new Exception("Account " + transactionRequest.getIban() + " does not exist");
        if (!tUtils.fundExists(transactionRequest.getFundName())) throw new Exception("Fund " + transactionRequest.getIban() + " does not exist");
        transactionRequest.setTransactionNumber(generateNewTransactionNumber());
        FundResponse fund = new FundResponse();
        AccountResponse account = new AccountResponse();
        Transaction transaction = transactionMapper.mapFromTransactionRequestToTransaction(transactionRequest, getFundById(fund.getFundIdByName(transactionRequest.getFundName())), getAccountById(account.getAccountIdByIban(transactionRequest.getIban())));
        return repository.addTransaction(transaction);
    }

    public String generateNewTransactionNumber() {
        while (true) {
            String newTNumber = tUtils.getNewTransactionNumber();
            try {
                Transaction transaction = repository.getTransactionByTNumber(newTNumber);
            } catch (DataAccessException e) {
                return newTNumber;
            }
        }
    }


}
