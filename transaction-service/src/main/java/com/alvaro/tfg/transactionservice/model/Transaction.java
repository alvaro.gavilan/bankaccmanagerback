package com.alvaro.tfg.transactionservice.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Transaction {

    private int id;

    private LocalDateTime date;

    private String transactionNumber;

    private double amount;

    private int accountId;

    private int fundId;

}
