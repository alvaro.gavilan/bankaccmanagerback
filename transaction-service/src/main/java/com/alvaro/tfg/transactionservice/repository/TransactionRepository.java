package com.alvaro.tfg.transactionservice.repository;

import com.alvaro.tfg.transactionservice.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TransactionRepository implements ITransactionRepository{

    @Autowired
    private JdbcTemplate jdbc;

    @Autowired
    private NamedParameterJdbcTemplate namedJdbc;

    public List<Transaction> getAllTransactions() {
        String query = "SELECT * FROM TRANSACTION";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Transaction.class));
    }

    public List<Transaction> getAllTransactionsByDate(String date) {
        String query = "SELECT * FROM TRANSACTION WHERE DATE LIKE ?";
        String dateRegex = date + "%";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Transaction.class), dateRegex);
    }

    public Transaction getTransactionById(int id) {
        String query = "SELECT * FROM TRANSACTION WHERE ID = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Transaction.class), id);
    }

    public Transaction getTransactionByTNumber(String tNumber) {
        String query = "SELECT * FROM TRANSACTION WHERE TRANSACTION_NUMBER = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Transaction.class), tNumber);
    }

    public List<Transaction> getTransactionsLessThan(int amount) {
        String query = "SELECT * FROM TRANSACTION WHERE AMOUNT < ?";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Transaction.class), amount);
    }

    public List<Transaction> getTransactionsMoreThan(int amount) {
        String query = "SELECT * FROM TRANSACTION WHERE AMOUNT > ?";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Transaction.class), amount);
    }

    public List<Transaction> getTransactionsByAccId(int id) {
        String query = "SELECT * FROM TRANSACTION WHERE ACCOUNT_ID = ?";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Transaction.class), id);
    }

    public List<Transaction> getTransactionsByFundId(int id) {
        String query = "SELECT * FROM TRANSACTION WHERE FUND_ID = ?";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Transaction.class), id);
    }

    public int addTransaction(Transaction transaction) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("transactionNumber", transaction.getTransactionNumber())
                .addValue("amount", transaction.getAmount())
                .addValue("accountId", transaction.getAccountId())
                .addValue("fundId", transaction.getFundId());
        String query = "INSERT INTO TRANSACTION (DATE, TRANSACTION_NUMBER, AMOUNT, ACCOUNT_ID, FUND_ID) VALUES(NOW(), :transactionNumber, :amount, :accountId, :fundId)";
        return namedJdbc.update(query, params);
    }


}
