package com.alvaro.tfg.transactionservice.mappers;

import com.alvaro.tfg.transactionservice.api.AccountResponse;
import com.alvaro.tfg.transactionservice.api.FundResponse;
import com.alvaro.tfg.transactionservice.api.TransactionRequestResponse;
import com.alvaro.tfg.transactionservice.model.Transaction;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class TransactionMapper {

    public TransactionRequestResponse mapFromTransactionToTransactionResponse(Transaction transaction, FundResponse fund, AccountResponse account) {
        TransactionRequestResponse transactionResponse = new TransactionRequestResponse();
        transactionResponse.setDate(transaction.getDate());
        transactionResponse.setTransactionNumber(transaction.getTransactionNumber());
        transactionResponse.setAmount(transaction.getAmount());
        transactionResponse.setIban(account.getIban());
        transactionResponse.setFundName(fund.getName());
        return transactionResponse;
    }

    public Transaction mapFromTransactionRequestToTransaction(TransactionRequestResponse transactionRequest, FundResponse fund, AccountResponse account) {
        Transaction transaction = new Transaction();
        transaction.setDate(transactionRequest.getDate());
        transaction.setTransactionNumber(transactionRequest.getTransactionNumber());
        transaction.setAmount(transactionRequest.getAmount());
        transaction.setAccountId(account.getAccountIdByIban(account.getIban()));
        transaction.setFundId(fund.getFundIdByName(fund.getName()));
        return transaction;
    }



}
