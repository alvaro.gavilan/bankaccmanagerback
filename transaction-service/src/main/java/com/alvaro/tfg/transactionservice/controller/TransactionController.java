package com.alvaro.tfg.transactionservice.controller;

import com.alvaro.tfg.transactionservice.api.TransactionRequestResponse;
import com.alvaro.tfg.transactionservice.model.Transaction;
import com.alvaro.tfg.transactionservice.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    @Autowired
    TransactionService service;

    @GetMapping("/all")
    public List<Transaction> getAllTransactions() {
        return service.getAllTransactions();
    }

    @GetMapping("/date/{date}")
    public ResponseEntity<Object> getAllTransactionByDate(@PathVariable String date) {
        try {
            return new ResponseEntity<>(service.getAllTransactionsByDate(date), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Object> getTransactionById(@PathVariable int id) {
        try {
            return new ResponseEntity<>(service.getTransactionById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Transaction with id " + id + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/transactionNumber/{tNumber}")
    public ResponseEntity<Object> getTransactionByTnumber(@PathVariable String tNumber) {
        try {
            return new ResponseEntity<>(service.getTransactionByTNumber(tNumber), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Transaction " + tNumber + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/amountLessThan/{amount}")
    public ResponseEntity<Object> getTransactionsLessThan(@PathVariable int amount) {
        try {
            return new ResponseEntity<>(service.getTransactionsLessThan(amount), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Transactions not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/amountMoreThan/{amount}")
    public ResponseEntity<Object> getTransactionsMoreThan(@PathVariable int amount) {
        try {
            return new ResponseEntity<>(service.getTransactionsMoreThan(amount), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Transactions not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/accountId/{accId}")
    public ResponseEntity<Object> getTransactionsByAccId(@PathVariable int accId) {
        try {
            return new ResponseEntity<>(service.getTransactionsByAccId(accId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Transaction with account ID " + accId + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/fundId/{fundId}")
    public ResponseEntity<Object> getTransactionsByFundId(@PathVariable int fundId) {
        try {
            return new ResponseEntity<>(service.getTransactionsByFundId(fundId), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Transaction with fund ID " + fundId + " not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<Object> addTransaction(@RequestBody TransactionRequestResponse transactionRequest) {
        try {
            if (service.addTransaction(transactionRequest) == 1) {
                return new ResponseEntity<>("Transaction completed successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Transaction failed", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
