package com.alvaro.tfg.transactionservice.api;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TransactionRequestResponse {

    private LocalDateTime date;

    private String transactionNumber;

    private double amount;

    private String iban;

    private String fundName;

}
