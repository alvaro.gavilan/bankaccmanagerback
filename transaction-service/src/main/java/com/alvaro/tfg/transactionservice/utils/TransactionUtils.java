package com.alvaro.tfg.transactionservice.utils;

import com.alvaro.tfg.transactionservice.api.FundResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class TransactionUtils {

    private final RestTemplate REST_TEMPLATE = new RestTemplate();

    public boolean transactionNumberFormatOk(String tnumber) {
        Pattern pattern = Pattern.compile("^[a-zA-Z]{3}[0-9]{4}$");
        Matcher matcher = pattern.matcher(tnumber);
        return matcher.find();
    }

    public boolean accountExists(String iban) {
        String url = "http://localhost:8095/accounts/getAccountId/";
        int accId = REST_TEMPLATE.getForObject(url + iban, Integer.class);
        return accId > 0;
    }

    public boolean fundExists(String name) {
        String url = "http://localhost:8093/funds/getFundId/";
        int fundId = REST_TEMPLATE.getForObject(url + name, Integer.class);
        return fundId > 0;
    }

    public String generateRandomLetters(int n) {
        Random random = new Random();
        StringBuilder letters = new StringBuilder();
        for(int i = 0; i < n; i++) {
            char letter = (char) (random.nextInt(26) + 'A');
            letters.append(letter);
        }
        return letters.toString();
    }

    public String generateRandomNumbers(int n) {
        Random random = new Random();
        StringBuilder nums = new StringBuilder();
        for(int i = 0; i < n; i++) {
            int num = random.nextInt(10);
            nums.append(num);
        }
        return nums.toString();
    }

    public String getNewTransactionNumber() {
        return generateRandomLetters(3) + generateRandomNumbers(4);
    }


}
