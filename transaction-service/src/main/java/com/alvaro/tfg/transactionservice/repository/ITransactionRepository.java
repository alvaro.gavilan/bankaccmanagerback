package com.alvaro.tfg.transactionservice.repository;

import com.alvaro.tfg.transactionservice.model.Transaction;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITransactionRepository {

    public List<Transaction> getAllTransactions();

    public List<Transaction> getAllTransactionsByDate(String date);

    public Transaction getTransactionById(int id);

    public Transaction getTransactionByTNumber(String TNumber);

    public List<Transaction> getTransactionsLessThan(int amount);

    public List<Transaction> getTransactionsMoreThan(int amount);

    public List<Transaction> getTransactionsByAccId(int id);

    public List<Transaction> getTransactionsByFundId(int id);

    public int addTransaction(Transaction transaction);

}
