package com.alvaro.tfg.transactionservice.api;

import lombok.Data;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Data
public class AccountResponse {

    private String iban;

    private double balance;

    private boolean active;

    private String titular;

    private String titularIdentifier;

    private String currencyCode;

    public int getAccountIdByIban(String iban) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8095/accounts/getAccountId/";
        try {
            return restTemplate.getForObject(url + iban, Integer.class);
        } catch (HttpClientErrorException e) {
            return 0;
        }
    }

}
