package com.alvaro.tfg.transactionservice.api;

import lombok.Data;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

@Data
public class FundResponse {

    private String name;

    private String refNumber;

    private String currencyCode;

    private boolean active;

    private LocalDateTime activeDate;

    private LocalDateTime inactiveDate;

    public int getFundIdByName(String name) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8093/funds/getFundId/";
        try {
            return restTemplate.getForObject(url + name, Integer.class);
        } catch (HttpClientErrorException e) {
            return 0;
        }
    }

}
