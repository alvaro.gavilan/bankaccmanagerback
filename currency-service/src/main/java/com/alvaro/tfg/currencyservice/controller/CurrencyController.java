package com.alvaro.tfg.currencyservice.controller;

import com.alvaro.tfg.currencyservice.api.CurrencyRequestResponse;
import com.alvaro.tfg.currencyservice.model.Currency;
import com.alvaro.tfg.currencyservice.services.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/currencies")
public class CurrencyController {

    @Autowired
    private CurrencyService service;

    @GetMapping("/all")
    public ResponseEntity<Object> getAllCurrencies() {
        try{
            return new ResponseEntity<>(service.getAllCurrencies(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<Object> getCurrencyById(@PathVariable int id) {
        try {
            return new ResponseEntity<>(service.getCurrencyById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Currency not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<Object> getCurrencyByName(@PathVariable String name) {
        try {
            return new ResponseEntity<>(service.getCurrencyByName(name), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Currency not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/code/{code}")
    public ResponseEntity<Object> getCurrencyByCode(@PathVariable String code) {
        try {
            return new ResponseEntity<>(service.getCurrencyByCode(code), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Currency not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getCurrencyId/{code}")
    public ResponseEntity<Object> getCurrencyId(@PathVariable String code) {
        try {
            return new ResponseEntity<>(service.getCurrencyId(code), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Currency code not found", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/add")
    public ResponseEntity<String> addCurrency(@RequestBody CurrencyRequestResponse currencyRequest) {
        try {
            if (service.addCurrency(currencyRequest) == 1) {
                return new ResponseEntity<>("Currency successfully added", HttpStatus.OK);
            }
            return new ResponseEntity<>("Currency not added", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>("Currency not added: " + e.getCause(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateCurrency(@RequestBody CurrencyRequestResponse currencyRequest, @PathVariable int id) {
        try {
            if (service.updateCurrency(currencyRequest, id) == 1) {
                return new ResponseEntity<>("Currency updated successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Client not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteCurrency(@PathVariable int id) {
        try {
            if (service.deleteCurrency(id) == 1) {
                return new ResponseEntity<>("Currency deleted successfully", HttpStatus.OK);
            }
            return new ResponseEntity<>("Currency not found", HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getCause() + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
