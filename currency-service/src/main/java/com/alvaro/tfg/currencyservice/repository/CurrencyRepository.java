package com.alvaro.tfg.currencyservice.repository;

import com.alvaro.tfg.currencyservice.model.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CurrencyRepository implements ICurrencyRepository{

    @Autowired
    JdbcTemplate jdbc;

    @Autowired
    NamedParameterJdbcTemplate namedJdbc;

    public List<Currency> getAllCurrencies() {
        String query = "SELECT ID, NAME, SYMBOL, CODE FROM CURRENCY";
        return jdbc.query(query, new BeanPropertyRowMapper<>(Currency.class));
    }

    public Currency getCurrencyById(int id) {
        String query = "SELECT ID, NAME, SYMBOL, CODE FROM CURRENCY WHERE ID = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Currency.class), id);
    }

    public Currency getCurrencyByName(String name) {
        String query = "SELECT ID, NAME, SYMBOL, CODE FROM CURRENCY WHERE NAME = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Currency.class), name);
    }

    public Currency getCurrencyByCode(String code) {
        String query = "SELECT ID, NAME, SYMBOL, CODE FROM CURRENCY WHERE CODE = ?";
        return jdbc.queryForObject(query, new BeanPropertyRowMapper<>(Currency.class), code);
    }

    public int addCurrency(Currency currency) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", currency.getName())
                .addValue("symbol", currency.getSymbol())
                .addValue("code", currency.getCode());
        String query = "INSERT INTO CURRENCY (NAME, SYMBOL, CODE)" +
                "VALUES(:name, :symbol, :code)";
        return namedJdbc.update(query, params);
    }

    public int updateCurrency(Currency updatedCurrency, int id) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("name", updatedCurrency.getName())
                .addValue("symbol", updatedCurrency.getSymbol())
                .addValue("code", updatedCurrency.getCode())
                .addValue("id", id);
        String query = "UPDATE CURRENCY SET " +
                "NAME = :name, " +
                "SYMBOL = :symbol, " +
                "CODE = :code " +
                "WHERE ID = :id";
        return namedJdbc.update(query, params);
    }

    public int deleteCurrency(int id) {
        String query = "DELETE FROM CURRENCY WHERE ID = ?";
        return jdbc.update(query, id);
    }

}
