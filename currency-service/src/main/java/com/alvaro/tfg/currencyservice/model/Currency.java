package com.alvaro.tfg.currencyservice.model;

import lombok.Data;

@Data
public class Currency {

    private int id;

    private String name;

    private String symbol;

    private String code;

}
