package com.alvaro.tfg.currencyservice.services;

import com.alvaro.tfg.currencyservice.api.CurrencyRequestResponse;
import com.alvaro.tfg.currencyservice.mappers.CurrencyMapper;
import com.alvaro.tfg.currencyservice.model.Currency;
import com.alvaro.tfg.currencyservice.repository.ICurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrencyService {

    @Autowired
    private ICurrencyRepository repository;

    public List<Currency> getAllCurrencies() {
        return repository.getAllCurrencies();
    }

    public CurrencyRequestResponse getCurrencyById(int id){
        return CurrencyMapper.mapFromCurrencyToCurrencyResponse(repository.getCurrencyById(id));
    }

    public CurrencyRequestResponse getCurrencyByName(String name){
        return CurrencyMapper.mapFromCurrencyToCurrencyResponse(repository.getCurrencyByName(name));
    }

    public CurrencyRequestResponse getCurrencyByCode(String code){
        return CurrencyMapper.mapFromCurrencyToCurrencyResponse(repository.getCurrencyByCode(code));
    }

    public int getCurrencyId(String code) {
        Currency currency = repository.getCurrencyByCode(code);
        return currency.getId();
    }

    public int addCurrency(CurrencyRequestResponse currencyRequest) {
        return repository.addCurrency(CurrencyMapper.mapFromCurrencyRequestToCurrency(currencyRequest));
    }

    public int updateCurrency(CurrencyRequestResponse currencyRequest, int id) {
        return repository.updateCurrency(CurrencyMapper.mapFromCurrencyRequestToCurrency(currencyRequest), id);
    }

    public int deleteCurrency(int id) {
        return repository.deleteCurrency(id);
    }

}
