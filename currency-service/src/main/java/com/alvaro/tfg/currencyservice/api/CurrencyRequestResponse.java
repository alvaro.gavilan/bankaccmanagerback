package com.alvaro.tfg.currencyservice.api;

import lombok.Data;

@Data
public class CurrencyRequestResponse {

    private String name;

    private String symbol;

    private String code;

}
