package com.alvaro.tfg.currencyservice.mappers;

import com.alvaro.tfg.currencyservice.api.CurrencyRequestResponse;
import com.alvaro.tfg.currencyservice.model.Currency;

public class CurrencyMapper {

    public static CurrencyRequestResponse mapFromCurrencyToCurrencyResponse(Currency currency){
        CurrencyRequestResponse currencyResponse = new CurrencyRequestResponse();
        currencyResponse.setName(currency.getName());
        currencyResponse.setSymbol(currency.getSymbol());
        currencyResponse.setCode(currency.getCode());
        return currencyResponse;
    }

    public static Currency mapFromCurrencyRequestToCurrency(CurrencyRequestResponse currencyRequest) {
        Currency currency = new Currency();
        currency.setName(currencyRequest.getName());
        currency.setSymbol(currencyRequest.getSymbol());
        currency.setCode(currencyRequest.getCode());
        return currency;
    }

}
