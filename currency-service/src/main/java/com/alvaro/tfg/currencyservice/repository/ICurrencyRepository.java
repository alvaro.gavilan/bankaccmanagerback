package com.alvaro.tfg.currencyservice.repository;

import com.alvaro.tfg.currencyservice.model.Currency;

import java.util.List;

public interface ICurrencyRepository {

    public List<Currency> getAllCurrencies();

    public Currency getCurrencyById(int id);

    public Currency getCurrencyByName(String name);

    public Currency getCurrencyByCode(String code);

    public int addCurrency(Currency currency);

    public int updateCurrency(Currency updatedCurrency, int id);

    public int deleteCurrency(int id);
}
